<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->

<!-- Users, Roles, Permissions -->
@if(
    backpack_user()->can('Create users') ||
    backpack_user()->can('Edit users') ||
    backpack_user()->can('Delete users') ||
    backpack_user()->can('Change user passwords')
)
    <li class="nav-title">Administrators</li>
    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-group"></i> Authentication</a>
        <ul class="nav-dropdown-items">
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon fa fa-user"></i> <span>Users</span></a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon fa fa-group"></i> <span>Roles</span></a></li>
            @if(backpack_user()->can('Manage permission'))
                <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon fa fa-key"></i> <span>Permissions</span></a></li>
            @endif
        </ul>
    </li>
@endif
@if(backpack_user()->can('Manage content'))
    <li class="nav-title">Digital Content</li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i class='nav-icon fa fa-list'></i> Categories</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('article') }}'><i class='nav-icon fa fa-newspaper-o'></i> Articles</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('discussion') }}'><i class='nav-icon fa fa-stack-overflow'></i> Discussions</a></li>
@endif

<li class="nav-title">System</li>
