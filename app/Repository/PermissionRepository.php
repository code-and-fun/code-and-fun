<?php

namespace App\Repositories;

use Backpack\PermissionManager\app\Models\Permission;

class PermissionRepository extends BaseRepository
{
    public function model()
    {
        return Permission::class;
    }

    public function findOrCreate()
    {
        $permissionCreated = [];
        foreach (config('backpack.permissionmanager.permission') as $permission) {
            $permissionFound = $this->getByColumn($permission, 'name');
            if (!$permissionFound) {
                $permissionFound = $this->create(['name' => $permission, 'guard_name' => 'backpack']);
            }
            $permissionCreated[$permissionFound->name] = $permissionFound->id;
        }
        return $permissionCreated;
    }

    public function clearAndCreate()
    {
        $this->deleteAll();
        $permissionCreated = [];
        foreach (config('backpack.permissionmanager.permission') as $permission) {
            $permissionFound = $this->create(['name' => $permission, 'guard_name' => 'backpack']);
            $permissionCreated[$permissionFound->name] = $permissionFound->id;
        }
        return $permissionCreated;
    }

    public function deleteAll()
    {
        foreach ($this->all() as $permissionDelete) {
            $this->deleteById($permissionDelete->id);
        }
    }
}
