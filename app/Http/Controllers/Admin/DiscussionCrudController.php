<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DiscussionRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class DiscussionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DiscussionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
//    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
//    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Discussion');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/discussion');
        $this->crud->setEntityNameStrings(__('discussion'), __('discussions'));
    }

    protected function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'id',
                'label' => __('ID'),
                'type' => "text",
            ],
            [
                // 1-n relationship
                'label' => __('Category'), // Table column heading
                'type' => "select",
                'name' => 'category_id', // the column that contains the ID of that connected entity;
                'entity' => 'category', // the method that defines the relationship in your Model
                'attribute' => "name", // foreign key attribute that is shown to user
                'model' => "App\Models\Category", // foreign key model
            ],
            [
                'name' => 'title',
                'label' => __('Title'),
                'type' => "text",
            ],
            [
                // 1-n relationship
                'label' => __('Creator'), // Table column heading
                'type' => "select",
                'name' => 'creator', // the column that contains the ID of that connected entity;
                'entity' => 'creator', // the method that defines the relationship in your Model
                'attribute' => "name", // foreign key attribute that is shown to user
                'model' => "App\Models\User", // foreign key model
            ],
            [
                'name' => 'status',
                'label' => __('Status'),
                'type' => "checkbox",
            ],
            [
                'name' => 'description',
                'label' => __('Description'),
                'type' => "textarea",
            ],
            [
                'name' => 'content',
                'label' => __('Content'),
                'type' => "textarea",
            ],
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(DiscussionRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        $this->crud->setFromDb();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
