<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CategoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;


    public function setup()
    {
        $this->crud->setModel('App\Models\Category');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/category');
        $this->crud->setEntityNameStrings(__('category'), __('categories'));
    }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 2);
    }

    protected function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'name',
                'label' => __('Name'),
            ],
            [
                'label' => "Parent",
                'type' => "select",
                'name' => 'parent_id',
                'entity' => 'parent',
                'attribute' => "name",
                'model' => "App\Models\Category",
            ],
            [
                'name' => 'status',
                'label' => __('Status'),
                'type' => 'check'
            ]
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->addFields([
            [
                'name' => 'status',
                'label' => __('Active'),
                'type' => 'checkbox',
            ],
            [
                'name' => 'name',
                'label' => __('Name'),
                'type' => 'text',
            ],
            [  // Select
                'label' => __('Root Category'),
                'type' => 'select',
                'name' => 'parent_id', // the db column for the foreign key
                'entity' => 'parent', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => "App\Models\Category",

//                // optional
//                'options'   => (function ($query) {
//                    return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
//                }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            ]
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
