<?php

namespace App\Http\Controllers\Admin;

use App\Enum\PublicStatusEnum;
use App\Http\Requests\ArticleRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ArticleCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArticleCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation { store as traitStore; }
//    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Article');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/article');
        $this->crud->setEntityNameStrings(__('article'), __('articles'));
    }

    protected function setupListOperation()
    {
        $this->crud->addColumns([
            [
                'name' => 'id',
                'label' => __('ID'),
                'type' => "text",
            ],
            [
                // 1-n relationship
                'label' => __('Category'), // Table column heading
                'type' => "select",
                'name' => 'category_id', // the column that contains the ID of that connected entity;
                'entity' => 'category', // the method that defines the relationship in your Model
                'attribute' => "name", // foreign key attribute that is shown to user
                'model' => "App\Models\Category", // foreign key model
            ],
            [
                'name' => 'title',
                'label' => __('Title'),
                'type' => "text",
            ],
            [
                // 1-n relationship
                'label' => __('Creator'), // Table column heading
                'type' => "select",
                'name' => 'creator_id', // the column that contains the ID of that connected entity;
                'entity' => 'creator', // the method that defines the relationship in your Model
                'attribute' => "name", // foreign key attribute that is shown to user
                'model' => "App\Models\User", // foreign key model
            ],
            [
                'name' => 'status',
                'label' => __('Status'),
                'type' => "text",
            ],
            [
                'name' => 'description',
                'label' => __('Description'),
                'type' => "textarea",
            ],
            [
                'name' => 'content',
                'label' => __('Content'),
                'type' => "textarea",
            ],
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->addFields([
            [
                'name' => 'creator_id',
                'type' => 'hidden'
            ],
            [
                'label' => __('Title'),
                'name' => 'title'
            ],
            [   // radio
                'name'        => 'status', // the name of the db column
                'label'       => 'Status', // the input label
                'type'        => 'radio',
                'options'     => PublicStatusEnum::toArray(),
                // optional
                'inline'      => true, // show the radios all on the same line?
            ],
            [   // select2_nested
                'name' => 'category_id',
                'label' => __('Category'),
                'type' => 'select2_nested',
                'entity' => 'category', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model' => "App\Models\Category", // force foreign key model
            ],
            [
                'label' => __('Description'),
                'name' => 'description',
                'type' => 'ckeditor'
            ],
            [
                'label' => __('Content'),
                'name' => 'content',
                'type' => 'ckeditor'
            ],


        ]);
    }

    public function store()
    {
        $this->crud->request->request->add(['creator_id'=> backpack_user()->id]);

        return $this->traitStore();
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
