<?php

namespace App\Enum;


use MyCLabs\Enum\Enum;

class ActiveStatusEnum extends Enum {

    use ToOptions;

    const ACTIVE = 1;
    const INACTIVE = 0;
}
