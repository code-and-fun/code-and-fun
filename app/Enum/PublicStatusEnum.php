<?php

namespace App\Enum;


use MyCLabs\Enum\Enum;

class PublicStatusEnum extends Enum {

    use ToOptions;

    const DRAFT = 'DRAFT';
    const PUBLISHED = 'PUBLISHED';
}
