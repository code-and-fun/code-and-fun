<?php

namespace App\Console\Commands;


use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PermissionDataProcessCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:permission {--action=insert}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to process permission data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $action = $this->option('action');

        $permissionRepository = new PermissionRepository();
        $roleRepository = new RoleRepository();

        if ($action === 'insert') {

            $permissionCreated = $permissionRepository->findOrCreate();
            $roleCreated = $roleRepository->findOrCreate();
            $this->setRoleAndPermissionRelation($permissionCreated, $roleCreated, 'insert');
            $this->info('Insert role and permission to database successfully!');
        } elseif ($action === 'reset') {
                $permissionCreated = $permissionRepository->clearAndCreate();
                $roleCreated = $roleRepository->clearAndCreate();
                $this->setRoleAndPermissionRelation($permissionCreated, $roleCreated, 'reset');
                $this->info('Reset role and permission to database successfully!');

        } else {
            $this->error('Action is not existed!');
        }
    }

    private function setRoleAndPermissionRelation($permissionCreated, $roleCreated, $setAction)
    {
        if ($setAction === 'reset') {
            DB::table('role_has_permissions')->truncate();
        }
        foreach (config('backpack.permissionmanager.role_has_permission') as $role => $permissions) {
            if ($permissions === '*') {
                $permissions = config('backpack.permissionmanager.permission');
            }
            foreach ($permissions as $permission) {
                if (
                    $setAction === 'insert' &&
                    DB::table('role_has_permissions')->where([
                        'permission_id' => $permissionCreated[$permission],
                        'role_id' => $roleCreated[$role],
                    ])->first()
                ) {
                    continue;
                }
                DB::table('role_has_permissions')->insert([
                    'permission_id' => $permissionCreated[$permission],
                    'role_id' => $roleCreated[$role],
                ]);
            }
        }
    }
}
