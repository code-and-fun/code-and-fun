<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->nullable();
            $table->unsignedBigInteger('creator_id');
            $table->unsignedInteger('category_id')->default(0);
            $table->string('title');
            $table->string('slug');
            $table->text('content');
            $table->text('description')->nullable();
            $table->string('status', 30)->default('DRAFT')->comment('draft, published');

            $table->integer('count_like')->nullable()->default(0)->comment('đếm lượt like');
            $table->integer('count_reply')->nullable()->default(0)->comment('đếm lượt reply');
            $table->integer('count_view')->nullable()->default(0)->comment('đếm lượt view');

            $table->timestamp('published_at')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();

            $table->index('slug');
            $table->index('creator_id');
            $table->index('category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
