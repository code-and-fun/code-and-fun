<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscussionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discussions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->nullable();
            $table->integer('creator_id')->comment('id người tạo');
            $table->string('slug')->nullable();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->text('content')->nullable()->comment('nội dung');
            $table->text('content_markdown')->nullable()->comment('nội dung dạng markdown');
            $table->integer('category_id')->default(0);

            $table->integer('count_like')->nullable()->default(0)->comment('đếm lượt like');
            $table->integer('count_reply')->nullable()->default(0)->comment('đếm lượt reply');
            $table->integer('count_view')->nullable()->default(0)->comment('đếm lượt view');

            $table->integer('status')->default(0)->comment('1 là active , 0 là inactive');
            $table->timestamps();

            $table->index('category_id');
            $table->index('creator_id');
            $table->index('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discussions');
    }
}
