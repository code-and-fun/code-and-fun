const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.sass('resources/assets/frontend_v1/sass/screens/home.scss', 'public/frontend/css');

mix.copyDirectory('resources/assets/frontend_v1/image', 'public/frontend/image');
mix.copyDirectory('resources/assets/frontend_v1/fonts', 'public/frontend/fonts');
mix.copyDirectory('resources/assets/frontend_v1/packages', 'public/frontend/packages');
